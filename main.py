from flask import Flask, render_template, request  # pip install flask

# from flask_sqlalchemy import SQLAlchemy  # pip install flask-slqalchemy
import sqlite3

app = Flask(__name__)


@app.route("/")
def home():
    return render_template("landingPage.html")


@app.route("/Ueberblick")
def Ueberblick():
    return render_template("Ueberblick.html")


@app.rout("/FlugBuchen")
def FlugBuchen():
    return render_template("FlugBuchen.html")


@app.route("/kundendaten", methods=["POST", "GET"])
def kundendaten():
    if request.method == "POST":
        try:
            vn = request.form["vn"]
            nn = request.form["nn"]
            svnr = request.form["svnr"]
            wo = request.form["wo"]
            strs = request.form["strs"]
            hnr = request.form["hnr"]
            plz = request.form["plz"]
            tnr = request.form["tnr"]

            with sqlite3.connect("database.db") as con:
                cur = con.cursor()
                cur.execute(
                    "INSERT INTO personen (vorname, nachname, svnr, wohnort, strasse, hausnummer, plz, telefonnummer) VALUES (?,?,?,?,?,?,?,?)",
                    (vn, nn, svnr, wo, strs, hnr, plz, tnr),
                )

                con.commit()
                msg = "Record successfully added"
        except:
            con.rollback()
            msg = "error in insert operation"

        finally:
            return render_template("Ueberblick.html", msg=msg)
            con.close()


if __name__ == "__main__":
    app.run()
